import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import App from './App';
import Design1 from './componenets/design1';

import { routes } from './utils/paths';
export default function MyRoutes() {
  return (
    <div>
      <BrowserRouter>
        <Switch>
          <Route exact path={routes.home} component={App} />
          <Route exact path={routes.trello} component={Design1} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}
