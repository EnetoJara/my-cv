const routes = {
  home: '/',
  design1: '/trillo',
  pg404: '/404'
};

export { routes };
