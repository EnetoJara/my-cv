import React from 'react';

export default () => {
  return (
    <div className="w3-container w3-card w3-white w3-margin-bottom experiences-pimps">
      <h2 className="w3-text-grey w3-padding-16">
        <i className="fa fa-suitcase fa-fw w3-margin-right w3-xxlarge w3-text-teal" />
        Work Experience
      </h2>

      <div className="w3-container">
        <h5 className="w3-opacity">
          <b>
            Tiempo Development - Advanced Pricing "<small>Elephants</small>"
          </b>
        </h5>
        <h6 className="w3-text-teal">
          JavaScript Developer <br />
          <i className="fa fa-calendar fa-fw w3-margin-right" />
          Feb 2019 - <span className="w3-tag w3-teal w3-round">Current</span>
        </h6>
        <p>
          I work as a FullStack developer. We are migrating a legacy application into Azure.
          The giant monolithic system of an American Health Care Insurance Company is been decompose into self independent microservices.
          Each microservice is built on top of NestJS a Reactive NodeJS Framework. Postgres is the main DB Manager System.
          The React Single Page App connects to and Api gateway that orchestrates all the microservices.
        </p>
        <p className="bosses">
          <small>Dev Manager: To Be Continue....</small>
          <small>Scrum Master: To Be Continue....</small>
        </p>
        <hr />
      </div>



      <div className="w3-container">
        <h5 className="w3-opacity">
          <b>
            Nextiva - Integrations "<small>Gummy Bears</small>"
          </b>
        </h5>
        <h6 className="w3-text-teal">
          JavaScript Developer <br />
          <i className="fa fa-calendar fa-fw w3-margin-right" />
          Jan 2019 - Nov 2019
        </h6>
        <p>
        I Designed and Developed the Front-End Architecture from scratch of “Integrations”. Single page app built on React with Typescript. Always following best practices and implementing the Presentational and Container Component Pattern. As we applied separation of concerns, we end up having quite a few “extra” components to keep in track what component was in charge of what we made used of a tool call TypeDoc to build a wiki for the code. The main purpose of integrations is as the name says. We integrated our CRM with many third-party vendors, such as google maps, quickbooks, outlook and many more.
Redux is implemented as the state manager. To ensure the scalability of the system we implemented the Redux Ducks Pattern. By nature of the app to be able to connect with different type of sources we implemented redux-saga to handle high level of concurrency we applied the Generator – Promise Pattern.
Integrations is built with a mobile first approach and using the 7 in 1 architecture styling pattern of SASS/SCSS.

        </p>
        <div className="w3-sidebar w3-bar-block w3-card">
            <h5 className="w3-bar-item w3-light-grey w3-text-teal">Technologies</h5>
            <span className="w3-bar-item">Webpack 4</span>
            <span className="w3-bar-item">@babel 7</span>
            <span className="w3-bar-item">Typescript</span>
            <span className="w3-bar-item">EcmaScript 9</span>
            <span className="w3-bar-item">SCSS</span>
            <span className="w3-bar-item">ReactJS</span>
            <span className="w3-bar-item">Redux</span>
            <span className="w3-bar-item">Redux Sagas</span>
        </div>
        <p className="bosses">
          <small>Dev Manager: luis.sencion@nextiva.com</small>
          <small>Scrum Master: alejandro.gomez@nextiva.com</small>
        </p>
        <hr />
      </div>

      <div className="w3-container">
        <h5 className="w3-opacity">
          <b>
            Nextiva - Billing Platform "<small>Pandas</small>"
          </b>
        </h5>
        <h6 className="w3-text-teal">
          JavaScript Developer <br />
          <i className="fa fa-calendar fa-fw w3-margin-right" />
          Aug 2018 - Jan 2019
        </h6>
        <p>
        Billing Platform is a Single page app built on react from scratch. Always following best practices and implementing the Presentational and Container Component Pattern. Billing Platform is built with a mobile first approach and using the 7 in 1 architecture styling pattern of SASS/SCSS. The State manager implemented is Redux and Redux-Thunk to handle asynchronous tasks.
Billing Platform allows the clients of Nextiva to pay their bills as well as to purchase new services/products.

        </p>
        <p className="bosses">
          <small>Dev Manager: luis.sencion@nextiva.com</small>
          <small>Scrum Master: alejandro.gomez@nextiva.com</small>
        </p>
        <hr />
      </div>

      <div className="w3-container">
        <h5 className="w3-opacity">
          <b>
            IBM - PTI "<small>Black Leopards</small>"
          </b>
        </h5>
        <h6 className="w3-text-teal">
          Javascript developer. <br />
          <i className="fa fa-calendar fa-fw w3-margin-right" />
          Oct/10/2016 - May 2018
        </h6>
        <p>
          As a developer at PTI an internal application of IBM for pricing and
          costing the products that IBM sells. We developed a responsive SPA on
          AngularJS & Angular Material, which was consuming a rest API builded
          on NodeJS connected to a DB2 databse.
        </p>
        <p className="bosses">
          <small>Direct boss jose.guadalupe.garcia@ibm.com</small>
        </p>
        <hr />
      </div>

      <div className="w3-container">
        <h5 className="w3-opacity">
          <b>
            IBM - WWPRT "<small>Pandas</small>"
          </b>
        </h5>
        <h6 className="w3-text-teal">
          Java Jr developer. <br />
          <i className="fa fa-calendar fa-fw w3-margin-right" />
          Jun/01/2016 - Oct/10/2016
        </h6>
        <p>
          As a developer at WWPRT I calculated the price and cost of the
          products of that IBM sells, I used Java as main programing language,
          with it I used: Hibernate, Spring, DAOs, Managers for the backend to
          manage the information of a DB2 database. For the frontend I used
          struts, freemaker, Actions. I helped with one of the most important
          requirement for the Cost side of the application in the Saas Query
          Report.
        </p>
        <p className="bosses">
          <small>Direct boss jlperedo@mx1.ibm.com</small>
        </p>
        <hr />
      </div>
    </div>
  );
};
