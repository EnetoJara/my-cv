import React from 'react';
import { list } from './componenets/Skill/list';
import Skill from './componenets/Skill/Skill';
import enetito from './styles/images/photoSlack.jpg';

export default function Skills() {
  const MySkills = list.map(i => (
    <Skill key={i.id} skillName={i.skillName} percentage={i.percentage} />
  ));
  return (
    <div className="w3-white w3-text-grey w3-card-4 skills">
      <div className="w3-display-container">
        <img src={enetito} style={{ width: '100%' }} alt="Enetito" />
        <div className="w3-display-bottomleft w3-container w3-text-white">
          <h2>Ernesto Jara</h2>
        </div>
      </div>
      <div className="w3-container">
        <h5 className="w3-opacity">
          <b>Objective</b>
        </h5>
        <p>
          Develop myself in a company that offers prospects
          and professional development, as well as partnership and teamwork.
        </p>
        <hr />
      </div>
      <div className="w3-container">
        <p>
          <i className="fa fa-briefcase fa-fw w3-margin-right w3-large w3-text-teal" />
          JavaScript Software Developer
        </p>
        <p>
          <i className="fa fa-home fa-fw w3-margin-right w3-large w3-text-teal" />
          Guadalajara Jalisco
        </p>
        <p>
          <i className="fa fa-envelope fa-fw w3-margin-right w3-large w3-text-teal" />
          ernesto.jara06@gmail.com
        </p>
        <p>
          <i className="fa fa-phone fa-fw w3-margin-right w3-large w3-text-teal" />
          044 33 29 33 43 61
        </p>
        <h5 className="w3-opacity">
          <b>Attitude over Knowledge</b>
        </h5>
        <small>What you Know today, will be useless tomorrow </small>
        <hr />
        <p className="w3-large">
          <b>
            <i className="fa fa-asterisk fa-fw w3-margin-right w3-text-teal" />
            Skills
          </b>
        </p>
        {MySkills}
        <br />
        <p className="w3-large w3-text-theme">
          <b>
            <i className="fa fa-globe fa-fw w3-margin-right w3-text-teal" />
            Languages
          </b>
        </p>
        <p className="skill-name">English</p>
        <div className="w3-light-grey w3-round-xlarge">
          <div
            className="w3-round-xlarge w3-center w3-teal"
            style={{ height: '1.5rem', width: '85%' }}
          >
            85%
          </div>
        </div>
        <p className="skill-name">Spanish</p>
        <div className="w3-light-grey w3-round-xlarge">
          <div
            className="w3-round-xlarge w3-center w3-teal"
            style={{ height: '1.5rem', width: '100%' }}
          >
            Native
          </div>
        </div>
        <br />
      </div>
    </div>
  );
}
