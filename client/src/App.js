import React, { Component } from 'react';

import './styles/App.scss';
import WorkExperience from './WorkExperience';
import EducationExperience from './EducationExperience';
import Footer from './Footer';
import Skills from './Skills';

class App extends Component {
  render() {
    return (
      <div className="w3-light-grey">
        <div className="w3-row-padding">
          <div className="w3-third">
            <Skills />
            <br />
          </div>
          <div className="w3-twothird">
            <WorkExperience />
            <EducationExperience />
          </div>
        </div>

        <Footer />
      </div>
    );
  }
}

export default App;
