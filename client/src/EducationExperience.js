import React from 'react';
export default () => {
  return (
    <div className="w3-container w3-card w3-white educations">
      <h2 className="w3-text-grey w3-padding-16">
        <i className="fa fa-certificate fa-fw w3-margin-right w3-xxlarge w3-text-teal" />
        Education
      </h2>
      <div className="w3-container">
        <h5 className="w3-opacity">
          <b>Universidad Enrique Diaz de Leon</b>
        </h5>
        <h6 className="w3-text-teal">
          <i className="fa fa-calendar fa-fw w3-margin-right" />
          2012 - 2016
        </h6>
        <p>Degree in Software Engineering</p>
        <hr />
      </div>
      <div className="w3-container">
        <h5 className="w3-opacity">
          <b>Courses</b>
        </h5>
        <ul>
          <li>
            1st Congress of Software Engineering and its applications at Centro
            Universitario Enrique Díaz de León. 2014
          </li>
          <li>
            International Congress of investigation and innovation Software
            Engineering 2012 at UNIVA.
          </li>
          <li>PMI 6th Project Management Congress at ITESO 2013.</li>
          <li>IBM Agile Explorer</li>
          <li>IBM Design Thinking Practitioner</li>
          <li>IBM Watson Discovery Services Fundamentals Badge</li>
          <li>IBM Cognitive Partitioner Badge</li>
        </ul>
        <hr />
      </div>
      <div className="w3-container">
        <h5 className="w3-opacity">
          <b>Udemy Accounts</b>
        </h5>
        <p>
          <a href="https://www.udemy.com/user/ernesto-jara-olveda/">
            Ernesto Jara Olveda
          </a>
        </p>
        <p>
          <a href="https://www.udemy.com/user/ernesto-olveda/">
            Ernesto Olveda
          </a>
        </p>
        <br />
      </div>
      <div className="w3-container">
        <h5 className="w3-opacity">
          <b>Front End Masters</b>
        </h5>
        <p>
          <a href="https://frontendmasters.com/learn">Ernesto Jara Olveda</a>
        </p>
        <br />
      </div>
    </div>
  );
};
