import React from 'react';
import Aux from '../../Aux';

export default ({ skillName, percentage }) => {
  return (
    <Aux>
      <p className="skill-name">{skillName}</p>
      <div className="w3-light-grey w3-round-xlarge w3-small">
        <div
          className="w3-container w3-center w3-round-xlarge w3-teal"
          style={{ width: `${percentage}%` }}
        >
          {percentage}%
        </div>
      </div>
    </Aux>
  );
};
