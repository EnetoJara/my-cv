export const list = [
  { id: 1, skillName: 'JavaScript', percentage: 80 },
  { id: 2, skillName: 'Node JS', percentage: 80 },
  { id: 3, skillName: 'Nest JS', percentage: 70 },
  { id: 4, skillName: 'Express JS', percentage: 80 },
  { id: 5, skillName: 'Koa JS', percentage: 70 },
  { id: 6, skillName: 'Mongo DB', percentage: 70 },
  { id: 7, skillName: 'SQL', percentage: 90 },
  { id: 8, skillName: 'React JS', percentage: 90 },
  { id: 9, skillName: 'Angular 5', percentage: 80 },
  { id: 10, skillName: 'AngularJS', percentage: 65 },
  { id: 14, skillName: 'Webpack', percentage: 70 },
  { id: 15, skillName: 'Babel', percentage: 80 },
  { id: 16, skillName: 'TypeScript', percentage: 65 },
  { id: 11, skillName: 'SCSS', percentage: 90 },
  { id: 12, skillName: 'Java', percentage: 20 },
  { id: 13, skillName: 'C Sharp', percentage: 30 }
];
