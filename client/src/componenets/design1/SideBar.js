import React from 'react';
import { Link } from 'react-router-dom';
import { SomeIcon } from '../../utils/Icon';

export default function SideBar() {
  return (
    <aside className="trillo-sidebar">
      <ul className="trillo-side-nav">
        <li className="trillo-side-nav__item trillo-side-nav__item--active">
          <Link to="/" className="trillo-side-nav__link">
            <SomeIcon name="iconHome" clasName="trillo-side-nav__icon" />
            <span>Hotel</span>
          </Link>
        </li>
        <li className="trillo-side-nav__item">
          <Link to="/" className="trillo-side-nav__link">
            <SomeIcon
              name="iconAircreaftTakeOff"
              clasName="trillo-side-nav__icon"
            />
            <span>Flight</span>
          </Link>
        </li>
        <li className="trillo-side-nav__item">
          <Link to="/" className="trillo-side-nav__link">
            <SomeIcon name="iconKey" clasName="trillo-side-nav__icon" />
            <span>Car rental</span>
          </Link>
        </li>
        <li className="trillo-side-nav__item">
          <Link to="/" className="trillo-side-nav__link">
            <SomeIcon name="iconMap" clasName="trillo-side-nav__icon" />
            <span>Tours</span>
          </Link>
        </li>
      </ul>

      <div className="trillo-legal">
        {'#NiñosBien '}
        <span role="img" aria-label="niños bien">
          🐼👌
        </span>
      </div>
    </aside>
  );
}
