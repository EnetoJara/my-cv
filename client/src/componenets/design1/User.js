import React from 'react';

import user1 from '../../styles/images/user-1.jpg';
import user2 from '../../styles/images/user-2.jpg';
export default () => {
  return (
    <div className="trillo-user-reviews">
      <figure className="trillo-review">
        <blockquote className="trillo-review__text">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga
          doloremque architecto dicta animi, totam, itaque officia ex.
        </blockquote>
        <figcaption className="trillo-review__user">
          <img src={user1} alt="User 1" className="trillo-review__photo" />
          <div className="trillo-review__user-box">
            <p className="trillo-review__user-name">Nick Smith</p>
            <p className="trillo-review__user-date">Feb 23rd, 2017</p>
          </div>
          <div className="trillo-review__rating">7.8</div>
        </figcaption>
      </figure>

      <figure className="trillo-review">
        <blockquote className="trillo-review__text">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga
          doloremque architecto dicta animi.
        </blockquote>
        <figcaption className="trillo-review__user">
          <img src={user2} alt="User 1" className="trillo-review__photo" />
          <div className="trillo-review__user-box">
            <p className="trillo-review__user-name">Mary Thomas</p>
            <p className="trillo-review__user-date">Sept 13th, 2017</p>
          </div>
          <div className="trillo-review__rating">9.3</div>
        </figcaption>
      </figure>

      <button className="trillo-btn-inline">
        Show all <span>&rarr;</span>
      </button>
    </div>
  );
};
