import React from 'react';
import { SomeIcon } from '../../utils/Icon';

export default function Overview() {
  return (
    <section className="trillo-overview">
      <h1 className="trillo-overview__heading">Hotel Las Palmas</h1>

      <div className="trillo-overview__stars">
        <SomeIcon name="iconStar" clasName="trillo-overview__icon-star" />
        <SomeIcon name="iconStar" clasName="trillo-overview__icon-star" />
        <SomeIcon name="iconStar" clasName="trillo-overview__icon-star" />
        <SomeIcon name="iconStar" clasName="trillo-overview__icon-star" />
        <SomeIcon name="iconStar" clasName="trillo-overview__icon-star" />
        <SomeIcon name="iconStar" clasName="trillo-overview__icon-star" />
      </div>

      <div className="trillo-overview__location">
        <SomeIcon
          name="iconLocationPin"
          clasName="trillo-overview__icon-location"
        />

        <button className="btn-inline">Albufeira, Portugal</button>
      </div>

      <div className="trillo-overview__rating">
        <div className="trillo-overview__rating-average">8.6</div>
        <div className="trillo-overview__rating-count">429 votes</div>
      </div>
    </section>
  );
}
