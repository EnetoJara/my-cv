import React from 'react';
import Description from './Description';
import User from './User';

export default function Details() {
  return (
    <div className="trillo-detail">
      <Description />
      <User />
    </div>
  );
}
