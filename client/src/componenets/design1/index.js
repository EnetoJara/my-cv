import React from 'react';

import './sass/main.scss';
import Header from './header';
import SideBar from './SideBar';
import Main from './Main';
export default () => {
  return (
    <div className="trillo-container">
      <Header />
      <section className="trillo-content">
        <SideBar />
        <Main />
      </section>
    </div>
  );
};
