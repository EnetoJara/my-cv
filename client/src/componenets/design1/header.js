import React from 'react';

import user from '../../styles/images/user.jpg';

import logo from '../../styles/images/logo.png';
import { SomeIcon } from '../../utils/Icon';

export default function header() {
  return (
    <header className="trillo-header">
      <img src={logo} alt="trillo logo" className="trillo-logo" />

      <form action="#" className="trillo-search">
        <input
          type="text"
          className="trillo-search__input"
          placeholder="Search hotels"
        />
        <button className="trillo-search__button">
          <SomeIcon name="iconMagnifyingGlass" clasName="trillo-search__icon" />
        </button>
      </form>

      <nav className="trillo-user-nav">
        <div className="trillo-user-nav__icon-box">
          <SomeIcon name="iconBookmark" clasName="trillo-user-nav__icon" />
          <span className="trillo-user-nav__notification">7</span>
        </div>
        <div className="trillo-user-nav__icon-box">
          <SomeIcon name="iconChat" clasName="trillo-user-nav__icon" />
          <span className="trillo-user-nav__notification">13</span>
        </div>
        <div className="trillo-user-nav__user">
          <img src={user} alt="User" className="trillo-user-nav__user-photo" />
          <span className="trillo-user-nav__user-name">Ernesto</span>
        </div>
      </nav>
    </header>
  );
}
