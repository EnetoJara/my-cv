import React from 'react';

import user3 from '../../styles/images/user-3.jpg';
import user4 from '../../styles/images/user-4.jpg';
import user5 from '../../styles/images/user-5.jpg';
import user6 from '../../styles/images/user-6.jpg';
export default () => {
  return (
    <div className="trillo-description">
      <p className="trillo-paragraph">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi nisi
        dignissimos debitis ratione sapiente saepe. Accusantium cumque, quas, ut
        corporis incidunt deserunt quae architecto voluptate.
      </p>
      <p className="trillo-paragraph">
        Accusantium cumque, quas, ut corporis incidunt deserunt quae architecto
        voluptate delectus, inventore iure aliquid aliquam.
      </p>
      <ul className="trillo-list">
        <li className="trillo-list__item">Close to the beach</li>
        <li className="trillo-list__item">Breakfast included</li>
        <li className="trillo-list__item">Free airport shuttle</li>
        <li className="trillo-list__item">Free wifi in all rooms</li>
        <li className="trillo-list__item">Air conditioning and heating</li>
        <li className="trillo-list__item">Pets allowed</li>
        <li className="trillo-list__item">We speak all languages</li>
        <li className="trillo-list__item">Perfect for families</li>
      </ul>
      <div className="trillo-recommend">
        <p className="trillo-recommend__count">
          Lucy and 3 other friends recommend this hotel.
        </p>
        <div className="trillo-recommend__friends">
          <img src={user3} alt="Friend 1" className="trillo-recommend__photo" />
          <img src={user4} alt="Friend 2" className="trillo-recommend__photo" />
          <img src={user5} alt="Friend 3" className="trillo-recommend__photo" />
          <img src={user6} alt="Friend 4" className="trillo-recommend__photo" />
        </div>
      </div>
    </div>
  );
};
