import React from 'react';
import Gallery from './Gallery';
import Overview from './Overview';
import Details from './Details';

export default function Main() {
  return (
    <main className="trillo-hotel-view">
      <Gallery />
      <Overview />
      <Details />
    </main>
  );
}
