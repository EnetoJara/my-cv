import React from 'react';
import hotel1 from '../../styles/images/hotel-1.jpg';
import hotel2 from '../../styles/images/hotel-2.jpg';
import hotel3 from '../../styles/images/hotel-3.jpg';

export default function Gallery() {
  return (
    <section className="trillo-gallery">
      <figure className="trillo-gallery__item">
        <img src={hotel1} alt="hotel 1" className="trillo-gallery__photo" />
      </figure>
      <figure className="trillo-gallery__item">
        <img src={hotel2} alt="hotel 2" className="trillo-gallery__photo" />
      </figure>
      <figure className="trillo-gallery__item">
        <img src={hotel3} alt="hotel 3" className="trillo-gallery__photo" />
      </figure>
    </section>
  );
}
