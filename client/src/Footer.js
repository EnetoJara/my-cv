import React from 'react';

export default function() {
  return (
    <footer className="w3-container w3-teal w3-center w3-margin-top">
      <p>Find me on social media.</p>
      <p>
        <small>
          #NiñosBien{' '}
          <span role="img" aria-label="panda">
            🐼👌
          </span>
        </small>
      </p>

      <a href="https://medium.com/@enetoOlveda">
        <i className="fa fa-medium w3-hover-opacity" />
      </a>

      <a href="https://stackoverflow.com/users/story/7366526?view=Timeline">
        <i className="fa fa-stack-overflow w3-hover-opacity" />
      </a>

      <a href="https://github.com/EnetoJara">
        <i className="fa fa-github w3-hover-opacity" />
      </a>

      <a href="https://www.linkedin.com/in/ernesto-jara-olveda/">
        <i className="fa fa-linkedin w3-hover-opacity" />
      </a>

      <a href="https://www.facebook.com/thePimp06">
        <i className="fa fa-facebook-official w3-hover-opacity" />
      </a>

      <a href="https://www.instagram.com/ernestoolveda/?hl=es-la">
        <i className="fa fa-instagram w3-hover-opacity" />
      </a>

      <a href="https://twitter.com/JaraOlveda">
        <i className="fa fa-twitter w3-hover-opacity" />
      </a>

      <p>
        Powered by{' '}
        <a href="https://www.w3schools.com/w3css/default.asp">w3.css</a>
      </p>
    </footer>
  );
}
