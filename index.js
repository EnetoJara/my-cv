"use strict";

var express = require("express");
var bodyParser = require("body-parser");
var path = require("path");

const PORT = process.env.PORT || 5050;
const app = express();

const client = path.resolve(__dirname, "client", "index.html");

app.use(
	bodyParser.urlencoded({
		extended: false
	})
);

app.use(express.static("client"));
app.get("*", (req, res) => res.sendFile(client));

app.listen(PORT, () => console.log("Up and Running n.n"));
